<?php

require_once "../Controladores/productos.controlador.php";
require_once "../Modelos/productos.modelo.php";

 

class TablaProductos{

 	/*=============================================
 	 MOSTRAR LA TABLA DE PRODUCTOS
  	=============================================*/ 

	public function mostrarTablaProductos(){

		$item = null;
    	$valor = null;
    	$orden = "id";

  		$productos = ControladorProductos::ctrMostrarProductos($item, $valor, $orden);	

  		if(count($productos) == 0){

  			echo '{"data": []}';

		  	return;
  		}
		
  		$datosJson = '{
		  "data": [';
$arreglo_retorno = array();
		  for($i = 0; $i < count($productos); $i++){

		  	/*=============================================
 	 		TRAEMOS LA IMAGEN
  			=============================================*/ 

		  	$imagen = "<img src='".$productos[$i]["imagen"]."' width='40px'>";

		  	/*=============================================
 	 		TRAEMOS LA CATEGORÍA
  			=============================================*/ 

		  	 
		  	/*=============================================
 	 		STOCK
  			=============================================*/ 

  		 

		  	/*=============================================
 	 		TRAEMOS LAS ACCIONES
  			=============================================*/ 

  			 

  				 $botones =  "<div class='btn-group'><button class='btn btn-warning btnEditarProducto' idProducto='".$productos[$i]["id"]."' data-toggle='modal' data-target='#nuevomodaledti'><i class='fa fa-pencil'></i></button><button class='btn btn-danger btnEliminarProducto' idProducto='".$productos[$i]["id"]."'  imagen='".$productos[$i]["imagen"]."'><i class='fa fa-times'></i></button></div>"; 
 

		 
		  	$datosJson .='[
			      "'.($i+1).'",
			      "'.$imagen.'",
			      "'.$productos[$i]["nombre"].'",
			      "'.$productos[$i]["email"].'",
			      "'.$productos[$i]["extencion"].'",
			      "'.$productos[$i]["celular"].'",
			      "'.$productos[$i]["departamento"].'",
			      "'.$productos[$i]["cargo"].'",
			      "'.$productos[$i]["ciudad"].'",
			      "'.$botones.'"
			    ],';
                        
                        
                    $arreglo_interior = array($productos[$i]["nombre"],
                  $productos[$i]["nombre"],
                   $productos[$i]["nombre"],
                    $productos[$i]["nombre"],
                   $productos[$i]["nombre"],
                    $productos[$i]["nombre"],
                    $productos[$i]["nombre"],
                   $productos[$i]["nombre"],
                    $productos[$i]["nombre"],
                        $productos[$i]["nombre"]);
                array_push($arreglo_retorno, $arreglo_interior);
                        

		  }
/*
		  $datosJson = substr($datosJson, 0, -1);

		 $datosJson .=   '] 

		 }';
		
		echo $datosJson;
*/
$json = json_encode($arreglo_retorno);
echo $json;
	}



}

/*=============================================
ACTIVAR TABLA DE PRODUCTOS
=============================================*/ 
$activarProductos = new TablaProductos();
$activarProductos -> mostrarTablaProductos();

