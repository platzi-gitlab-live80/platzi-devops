<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="<?php echo htmlspecialchars(strip_tags('css/bootstrap-3.3.5-dist/css/bootstrap.css'));?>">
        <link rel="stylesheet" href="<?php echo htmlspecialchars(strip_tags('css/login7.css'));?>">
        <script src="<?php echo htmlspecialchars(strip_tags('js/jquery-2.1.4.js'));?>"></script>
        <script type="text/javascript" src="<?php echo htmlspecialchars(strip_tags('css/bootstrap-3.3.5-dist/js/bootstrap.js'));?>"></script>        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="text/javascript" src="<?php echo htmlspecialchars(strip_tags('lib/Usuario/js/Usuario.js'));?>"></script>
        <script type="text/javascript" src="<?php echo htmlspecialchars(strip_tags('js/Validaciones.js'));?>"></script>
        <script type="text/javascript" src="<?php echo htmlspecialchars(strip_tags('lib/Utilidades/js/Utilidades.js'));?>"></script>
        <title>TRAMITES</title>        
    </head>
    <script>
        $(function() {
            $("#txtUsuario").popover({placement: 'right', html: true, trigger: 'clic', title: 'Usuario: ', content: '<p>Ingrese el usuario proporcionado</p>'});
            $("#txtClave").popover({placement: 'right', html: true, trigger: 'clic', title: 'Contraseña: ', content: '<p>Ingrese la clave proporcionada</p>'});
        });
    </script>

    <body data-offset="50" data-target=".subnav" data-spy="scroll">
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="brand" href="#"><img src="<?php echo htmlspecialchars(strip_tags('images/logo-ica-page-principal.png'));?>"></a>
                    <a class="brand navbar-right" href="#"><img src="<?php echo htmlspecialchars(strip_tags('images/logo_minagricultura.png'));?>"></a>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        
        <!--Descripcion : Se crea una vista para el envio de datos
         * Creado Por :  Jennifer Cabiativa
         * Fecha de creación : 30 de enero de 2017     
         * Empresa : ICA
        -->
        <div class="container" id="InfoData" style="display: none;">
            <div class="row">
                <div class='col-md-3'></div>
                <div class="col-md-6">
                    <div class="login-box well">                        
                            <legend>Iniciar Sesión</legend>
                            <div class="form-group">
                                <label for="txtUsuario">Usuario</label>
                                <input type="text" name="usernameview" id="usernameview" class="form-control" placeholder="Usuario" value='' required="true"/>                                
                            </div>
                            <div class="form-group">
                                <label for="txtClave">Contraseña</label>
                                <input type="password" name="passview" id="passview" class="form-control" placeholder="Contraseña" value='' required="true"/>                                
                            </div>
                            <div class="form-group">
                                <button type="submit" name="btnIngresar" id="btnIngresar" class="btn btn-default btn-login-submit btn-block m-t-md" ><i class="glyphicon glyphicon-user"></i>&nbsp;Ingresar</button>
                            </div>
                            <span class='text-center'><a href="<?php echo htmlspecialchars(strip_tags('Olvido.php'));?>" class="text-sm">Olvidó su contraseña?</a></span>
                            <div class="form-group">
                                <p class="text-center m-t-xs text-sm">Desea registrarse?</p> 
                                <a href="<?php echo htmlspecialchars(strip_tags('registroPersona.php'));?>" class="btn btn-default btn-block m-t-md"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;<i class="glyphicon glyphicon-pencil">&nbsp;</i>&nbsp;Registro Persona</a>                                
                            </div>
                            <input type="hidden" value="Log" name="opcion" id="Log"/>  
                            <div id="messageLog"></div>                        
                    </div>
                </div>
                <div class='col-md-3'></div>
            </div>
        </div>
        <!--Fin Datos a mostrar-->
        
        <div class="container" id="data">
            <div class="row">
                <div class='col-md-3'></div>
                <div class="col-md-6">
                    <div class="login-box well">

                        <form method="POST" id="frm_login" action="<?php echo htmlspecialchars(strip_tags('lib/Usuario/Control/UsuarioController.php'));?>">
                            <legend>Iniciar Sesión</legend>
                            <div class="form-group">
                                <label for="txtUsuario">Usuario</label>
                                <input type="text" name="username" id="username" class="form-control" placeholder="Usuario" value='' required="true"/>                                
                            </div>
                            <div class="form-group">
                                <label for="txtClave">Contraseña</label>
                                <input type="password" name="pass" id="pass" class="form-control" placeholder="Contraseña" value='' required="true"/>                                
                            </div>
                            <div class="form-group">
                                <button type="submit" name="btnIngresar" id="btnIngresar" class="btn btn-default btn-login-submit btn-block m-t-md" onclick="encriptar('pass');
                                        encriptar('username');"><i class="glyphicon glyphicon-user"></i>&nbsp;Ingresar</button>
                            </div>
                            <span class='text-center'><a href="<?php echo htmlspecialchars(strip_tags('Olvido.php'));?>" class="text-sm">Olvidó su contraseña?</a></span>
                            <div class="form-group">
                                <p class="text-center m-t-xs text-sm">Desea registrarse?</p> 
                                <a href="<?php echo htmlspecialchars(strip_tags('registroPersona.php'));?>" class="btn btn-default btn-block m-t-md"><i class="glyphicon glyphicon-list-alt"></i>&nbsp;<i class="glyphicon glyphicon-pencil">&nbsp;</i>&nbsp;Registro Persona</a>                                
                            </div>
                            <input type="hidden" value="Log" name="opcion"   id="Log"/>  
                            <div id="messageLog"></div>
                        </form>
                    </div>
                </div>
                <div class='col-md-3'></div>
            </div>
        </div>
        <div class="navbar navbar-default navbar-fixed-bottom">
            <div class="container">
                <p class="navbar-text navbar-left">
                    <b>
                        INSTITUTO COLOMBIANO AGROPECUARIO – ICA<br>
                        Carrera 41 No. 17-81 · Bogotá – Colombia · Tel:&nbsp; (57 1) 332 3700 - 288 4800<br>
                        Correo electrónico: contactenos@ica.gov.co <br>
                        Horario de atención: Lunes a Viernes de 7:30 a.m. a 4:30 p.m. Jornada Continua<br>                        
                    </b>
                </p>
            </div>
        </div>
    </body>      
</html>


