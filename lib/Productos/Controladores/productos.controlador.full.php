<?php

 require_once "../Modelos/productos.modelo.php";
 require_once "../../../extensiones/vendor/autoload.php";
$opcion = filter_var(trim($_POST['opcion']),FILTER_SANITIZE_STRING);

if($opcion=='insert'){
  if(isset($_POST["nuevoNombreC"])){

			if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoNombreC"])){

		   		/*=============================================
				VALIDAR IMAGEN
				=============================================*/

			   	$ruta = "vistas/img/productos/default/anonymous.png";

			   	if(isset($_FILES["nuevaImagenes"]["tmp_name"])){

					list($ancho, $alto) = getimagesize($_FILES["nuevaImagenes"]["tmp_name"]);

					$nuevoAncho = 900;
					$nuevoAlto = 1200;

					/*=============================================
					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
					=============================================*/

					$directorio = "../vistas/img/productos/".$_POST["nuevoNombreC"];
                                       // echo "directorio_".$directorio;
					mkdir($directorio, 0755);

					/*=============================================
					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP
					=============================================*/

					if($_FILES["nuevaImagenes"]["type"] == "image/jpeg"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "../vistas/img/productos/".$_POST["nuevoNombreC"]."/".$aleatorio.".jpg";

						$origen = imagecreatefromjpeg($_FILES["nuevaImagenes"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagejpeg($destino, $ruta);

					}

					if($_FILES["nuevaImagenes"]["type"] == "image/png"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "../vistas/img/productos/".$_POST["nuevoNombreC"]."/".$aleatorio.".png";

						$origen = imagecreatefrompng($_FILES["nuevaImagenes"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagepng($destino, $ruta);

					}

				}

				$tabla = "productos";

                                
                                
                                /*
                                 *                                  * 
                                 * 

                                 */
				$datos = array( 
							   "imagen" => $ruta,
                                                            "nombre" => $_POST["nuevoNombreC"],
							   "email" => $_POST["nuevoCorreo"],
							   "extencion" => $_POST["nuevoExtencion"],
							   "celular" => $_POST["nuevoCelular"],
                                                           "fecha_nacimiento" => $_POST["nuevoFechaNacimiento"],
							   "cargo" => $_POST["nuevoCargo"],
							   "departamento" => $_POST["nuevoDepartamento"],
							   "ciudad" => $_POST["nuevoCiudad"]
                                    );

				$respuesta = ModeloProductos::mdlIngresarProducto($tabla, $datos);
//window.history.back();
				if($respuesta == "ok"){

					echo'<script>
                                            alert("Registro Actualizado");
					 window.location = "../index.php"
                                                 
 

                                                    

						</script>';

				}


			}else{

				echo'<script>

					swal({
						  type: "error",
						  title: "¡El registro de persona no puede ir con los campos vacíos o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonText: "Cerrar"
						  }).then(function(result){
							if (result.value) {

							window.location = "index.php";

							}
						})

			  	</script>';
			}
		}

	 }
         if($opcion=='update'){
             
             
             if(isset($_POST["nuevoNombreCEDT"])){
                    //comienzo de condicion    
                    	     if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoNombreCEDT"])){

				/*=============================================
				VALIDAR IMAGEN
				=============================================*/

				$ruta = $_POST["imagenActual"];

				if(isset($_FILES["editarImagen"]["tmp_name"]) && !empty($_FILES["editarImagen"]["tmp_name"])){

					list($ancho, $alto) = getimagesize($_FILES["editarImagen"]["tmp_name"]);

					$nuevoAncho = 900;
					$nuevoAlto = 1200;

					/*=============================================
					CREAMOS EL DIRECTORIO DONDE VAMOS A GUARDAR LA FOTO DEL USUARIO
					=============================================*/

					$directorio = "vistas/img/productos/".$_POST["nuevoNombreCEDT"];

					/*=============================================
					PRIMERO PREGUNTAMOS SI EXISTE OTRA IMAGEN EN LA BD
					=============================================*/

					if(!empty($_POST["imagenActual"])){

						unlink($_POST["imagenActual"]);

					}else{

						mkdir($directorio, 0755);

					}	

					/*=============================================
					DE ACUERDO AL TIPO DE IMAGEN APLICAMOS LAS FUNCIONES POR DEFECTO DE PHP
					=============================================*/

					if($_FILES["editarImagen"]["type"] == "image/jpeg"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/productos/".$_POST["nuevoNombreCEDT"]."/".$aleatorio.".jpg";

						$origen = imagecreatefromjpeg($_FILES["editarImagen"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagejpeg($destino, $ruta);

					}

					if($_FILES["editarImagen"]["type"] == "image/png"){

						/*=============================================
						GUARDAMOS LA IMAGEN EN EL DIRECTORIO
						=============================================*/

						$aleatorio = mt_rand(100,999);

						$ruta = "vistas/img/productos/".$_POST["nuevoNombreCEDT"]."/".$aleatorio.".png";

						$origen = imagecreatefrompng($_FILES["editarImagen"]["tmp_name"]);						

						$destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

						imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);

						imagepng($destino, $ruta);

					}

				}

				     
				$tabla = "productos";

				$datos = array( 
							   "imagen" => $ruta,
                                                           "codico_persona_pk" => $_POST["codico_persona_pk"],
                                                           "nombre" => $_POST["nuevoNombreCEDT"],
							   "email" => $_POST["nuevoCorreoEDT"],
							   "extencion" => $_POST["nuevoExtencionEDT"],
							   "celular" => $_POST["nuevoCelularEDT"],
                                                           "fecha_nacimiento" => $_POST["nuevoFechaNacimientoEDT"],
							   "cargo" => $_POST["nuevoCargoEDT"],
							   "departamento" => $_POST["nuevoDepartamentoEDT"],
							   "ciudad" => $_POST["nuevoCiudadEDT"]
                                                            );

				$respuesta = ModeloProductos::mdlEditarProducto($tabla, $datos);

				if($respuesta == "ok"){

					echo'<script>

						swal({
							  type: "success",
							  title: "El registro de persona ha sido editado correctamente",
							  showConfirmButton: true,
							  confirmButtonText: "Cerrar"
							  }).then(function(result){
										if (result.value) {

										window.location = "index.php";

										}
									})

						</script>';

				}


			}
                        
                    
                    
                    
                    //fin de condicion 
		 
		}

             
         }